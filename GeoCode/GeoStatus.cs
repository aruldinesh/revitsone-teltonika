﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genius.Google.Net
{

    /// <summary>
    /// 
    /// </summary>
    public enum GeoStatus {
        OK,
        ZERO_RESULTS,
        OVER_QUERY_LIMIT,
        REQUEST_DENIED,
        INVALID_REQUEST
    }
}
