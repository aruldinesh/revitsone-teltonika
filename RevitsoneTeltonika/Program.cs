﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using System.Net;
using System.Configuration;
using System.Net.Sockets;
using System.Threading;

namespace TCPListener
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger("");

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            TcpServerAsync().Wait();
        }

        private static async Task TcpServerAsync()
        {
            IPAddress ip;
            if (!IPAddress.TryParse(ConfigurationManager.AppSettings["ipAddress"], out ip))
            {
                Console.WriteLine("Failed to get IP address, service will listen for client activity on all network interfaces.");
                ip = IPAddress.Any;
            }

            int port;
            if (!int.TryParse(ConfigurationManager.AppSettings["port"], out port))
            {
                throw new ArgumentException("Port is not valid.");
            }

            Log.Info("Starting Teltonika Listener...");
            var server = new TcpListener(ip, port);

            server.Start();
            Log.Info("Listening...");
            while (true)
            {
                TcpClient client = await server.AcceptTcpClientAsync();
                var cw = new TcpClientService(client);
                ThreadPool.UnsafeQueueUserWorkItem(x => ((TcpClientService)x).Run(), cw);
            }
        }
    }
}
