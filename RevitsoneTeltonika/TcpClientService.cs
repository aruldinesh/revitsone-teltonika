﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using log4net;
using Teltonika.Codec;
using Teltonika.Codec.Model;
using CommonLibrary;
using System.Text;
using TCPListener.Helpers;
using System.Configuration;

namespace TCPListener
{
    public class TcpClientService
    {
        DataAccess dataAccess;
        ProtocolModelView protocolModelView;
        ProtocolStatusInfo protocolStatusInfo;
        private static readonly ILog Log = LogManager.GetLogger("");
        readonly TcpClient _client;

        public TcpClientService(TcpClient client)
        {
            _client = client;
            protocolModelView = new ProtocolModelView();
            protocolStatusInfo = new ProtocolStatusInfo();
            dataAccess = new DataAccess();
        }

        public async Task Run()
        {
            using (_client)
            using (var stream = _client.GetStream())
            {
                Log.Info(DateTime.Now + " Received connection request from " + _client.Client.RemoteEndPoint);
                string imeiNumber = "";
                var fullPacket = new List<byte>();
                int? avlDataLength = null;

                var bytes = new byte[4096];
                var connected = false;
                int length;

                // Loop to receive all the data sent by the client.
                while ((length = await stream.ReadAsync(bytes, 0, bytes.Length)) != 0)
                {
                    Log.Info(string.Format("{0} - received [{1}]", DateTime.Now, String.Join("", bytes.Take(length).Select(x => x.ToString("X2")).ToArray())));
                    
                    
                    byte[] response = null;
                    //
                    if (!connected)
                    {
                        // Accept imei
                        response = new byte[] { 01 };
                        connected = true;
                        await stream.WriteAsync(response, 0, response.Length);
                        
                        string[] imei = bytes.Take(length).Select(x => x.ToString("X2")).ToArray();

                        if (imei.Length > 0)
                        {
                            string imeiLength = imei[0] + imei[1];
                            int intValue = int.Parse(imeiLength, System.Globalization.NumberStyles.HexNumber);

                            if (intValue > 0)
                            {
                                StringBuilder deviceIMEI = new StringBuilder();
                                for (int i = 2; i < imei.Length; i++)
                                {
                                    string imeiPart = imei[i];
                                    imeiPart = imeiPart.Substring(1, 1);
                                    deviceIMEI.Append(imeiPart);
                                }
                                imeiNumber = deviceIMEI.ToString();
                            }
                        }
                        //string 
                        //int i = BitConverter.ToInt32(imei, 0);

                        Array.Clear(bytes, 0, bytes.Length);

                        Log.Info(string.Format("{0} - responded [{1}]", DateTime.Now, String.Join("", response.Select(x => x.ToString("X2")).ToArray())));
                    }
                    else
                    {
                        fullPacket.AddRange(bytes.Take(length));
                        Array.Clear(bytes, 0, bytes.Length);

                        var count = fullPacket.Count;

                        // continue if there is not enough bytes to get avl data array length
                        if (count < 8) continue;

                        avlDataLength = avlDataLength ?? BytesSwapper.Swap(BitConverter.ToInt32(fullPacket.GetRange(4, 4).ToArray(), 0));

                        var packetLength = 8 + avlDataLength + 4;
                        if (count > packetLength)
                        {
                            Log.Error("Too much data received.");
                            throw new ArgumentException("Too much data received.");
                        }
                        // continue if not all data received
                        if (count != packetLength) continue;

                        // Decode tcp packet
                        var decodedData = DecodeTcpPacket(fullPacket.ToArray());
                        response = BitConverter.GetBytes(BytesSwapper.Swap(decodedData.AvlData.DataCount));

                        await stream.WriteAsync(response, 0, response.Length);

                        avlDataLength = null;
                        fullPacket.Clear();
                        IEnumerable<AvlData> avlData = decodedData.AvlData.Data;
                        foreach (var item in avlData)
                        {
                            double lat = Math.Round(item.GpsElement.X, 6);
                            Console.WriteLine(lat);

                            Log.Info("Lat : " + item.GpsElement.X + ", " +" Lang : " +  item.GpsElement.Y);
                            //TODO:Call the insert function Here

                            //item.GpsElement
                            if (imeiNumber != "")
                            {
                                protocolModelView.StartByte = "2A2A";
                                protocolModelView.PacketLegnth = packetLength.ToString();
                                protocolModelView.LAC = "0000";
                                protocolModelView.DeviceID = imeiNumber;
                                protocolModelView.InfoSerialNo = "0300";
                                protocolModelView.ProtocolNumber = "10";
                                protocolModelView.DateTime = item.DateTime.ToString();
                                int latInt = Convert.ToInt32(item.GpsElement.X);
                                int lngInt = Convert.ToInt32(item.GpsElement.Y);

                                string latString = latInt.ToString();
                                string lngString = lngInt.ToString();

                                int testlat = -768916160;
                                string testla = testlat.ToString();

                                string latitude = latString.Substring(0, latString.Length - 7) + "." + latString.Substring(latString.Length - 7, latString.Length - latString.Substring(0, latString.Length - 7).Length); ;
                                string longitude = lngString.Substring(0, lngString.Length - 7) + "." + lngString.Substring(lngString.Length - 7, lngString.Length - lngString.Substring(0, lngString.Length - 7).Length); ;    

                                //string latRevString = string.re latInt.re
                                //string lngRevString = latInt.ToString();

                                //string latitude = latString.Remove(latString.Length-7)
                                //latString = latString.Substring(latString.Length - 7);

                                protocolModelView.Latitude = latitude;//item.GpsElement.X.ToString();
                                protocolModelView.Longitude = longitude;//item.GpsElement.Y.ToString();
                                protocolModelView.Speed = item.GpsElement.Speed.ToString();
                                protocolModelView.Course = item.GpsElement.Angle.ToString();
                                protocolModelView.MNC = "00";
                                protocolModelView.CellID = "3000";
                                protocolModelView.StatusByte = "00000000000000000000000000001111";
                                protocolModelView.GSMSignalStregnth = "17";
                                protocolModelView.VoltageLevel = "28";
                                protocolModelView.HDOP = "08";
                                protocolModelView.ADCValue = "0A00";
                                protocolModelView.OdometerIndex = "00";
                                protocolModelView.OdometerLegnth = "01";
                                protocolModelView.OdometerReading = "0500000264";
                                protocolModelView.RFIDIndex = "28";
                                protocolModelView.RFIDTag = "0205000000";
                                protocolModelView.StopByte = "0000";


                                IEnumerable<IoProperty> propertyList = item.IoElement.Properties;
                                foreach (var propertyListItem in propertyList)
                                {
                                    if (propertyListItem.Id == 252 && propertyListItem.Value == 1)
                                    {
                                        string sendTo = ConfigurationManager.AppSettings["SendTo"].ToString();
                                        string sendToCC = ConfigurationManager.AppSettings["SendToCC"].ToString();
                                        CommonClass commonClass = new CommonClass();
                                        commonClass.SendEmail(sendTo, sendToCC, "Device removed - " + imeiNumber, "Device removed - " + imeiNumber);
                                    }
                                }

                                string statusreve = DBHelper.ReverseString(protocolModelView.StatusByte);
                                protocolStatusInfo = new ProtocolStatusInfo();
                                protocolStatusInfo = GetStatusInfo(statusreve);

                                if (protocolModelView != null && protocolStatusInfo != null)
                                {
                                    dataAccess.InsertTCPMessage(protocolModelView, protocolStatusInfo);
                                }
                            }
                        }
                        

                        Log.Info(string.Format("{0} - responded [{1}]", DateTime.Now, String.Join("", response.Select(x => x.ToString("X2")).ToArray())));



                        //TODO:Make the following code into function
                        
                        

                        

                        /* 
                         *  model.StartByte = HexMessage.Substring(0, (int)Transync.StartByte * 2);
                        index += (int)Transync.StartByte * 2;

                        model.PacketLegnth = HexMessage.Substring(index, (int)Transync.PacketLegnth * 2);
                        index += (int)Transync.PacketLegnth * 2;

                        model.LAC = HexMessage.Substring(index, (int)Transync.LAC * 2);
                        index += (int)Transync.LAC * 2;

                        model.DeviceID = (HexMessage.Substring(index, (int)Transync.DeviceID * 2)).TrimStart(new char[] { '0' });
                        index += (int)Transync.DeviceID * 2;

                        model.InfoSerialNo = HexMessage.Substring(index, (int)Transync.InfoSerialNo * 2);
                        index += (int)Transync.InfoSerialNo * 2;

                        model.ProtocolNumber = HexMessage.Substring(index, (int)Transync.ProtocolNumber * 2);
                        index += (int)Transync.ProtocolNumber * 2;

                        model.DateTime = DBHelper.HextoDatetime(HexMessage.Substring(index, (int)Transync.DateTime * 2));
                        index += (int)Transync.DateTime * 2;

                        model.Latitude = Convert.ToString(DBHelper.ParseHexDegrees(HexMessage.Substring(index, (int)Transync.Latitude * 2)));
                        index += (int)Transync.Latitude * 2;

                        model.Longitude = Convert.ToString(DBHelper.ParseHexDegrees(HexMessage.Substring(index, (int)Transync.Longitude * 2)));
                        index += (int)Transync.Longitude * 2;

                        model.Speed = Convert.ToString(DBHelper.ParseHexSpeed(HexMessage.Substring(index, (int)Transync.Speed * 2)));
                        index += (int)Transync.Speed * 2;

                        model.Course = HexMessage.Substring(index, (int)Transync.Course * 2);
                        index += (int)Transync.Course * 2;

                        model.MNC = HexMessage.Substring(index, (int)Transync.MNC * 2);
                        index += (int)Transync.MNC * 2;

                        model.CellID = HexMessage.Substring(index, (int)Transync.CellID * 2);
                        index += (int)Transync.CellID * 2;

                        model.StatusByte = DBHelper.HexStringToBinary(HexMessage.Substring(index, (int)Transync.StatusByte * 2));
                        index += (int)Transync.StatusByte * 2;

                        model.GSMSignalStregnth = HexMessage.Substring(index, (int)Transync.GSMSignalStregnth * 2);
                        index += (int)Transync.GSMSignalStregnth * 2;

                        model.VoltageLevel = HexMessage.Substring(index, (int)Transync.VoltageLevel * 2);
                        index += (int)Transync.VoltageLevel * 2;

                        model.HDOP = HexMessage.Substring(index, (int)Transync.HDOP * 2);
                        index += (int)Transync.HDOP * 2;

                        model.ADCValue = HexMessage.Substring(index, (int)Transync.ADCValue * 2);
                        index += (int)Transync.ADCValue * 2;

                        model.OdometerIndex = HexMessage.Substring(index, (int)Transync.OdometerIndex * 2);
                        index += (int)Transync.OdometerIndex * 2;

                        model.OdometerLegnth = HexMessage.Substring(index, (int)Transync.OdometerLegnth * 2);
                        index += (int)Transync.OdometerLegnth * 2;

                        model.OdometerReading = HexMessage.Substring(index, (int)Transync.OdometerReading * 2);
                        index += (int)Transync.OdometerReading * 2;

                        model.RFIDIndex = HexMessage.Substring(index, (int)Transync.RFIDIndex * 2);
                        index += (int)Transync.RFIDIndex * 2;

                        model.RFIDTag = HexMessage.Substring(index, (int)Transync.RFIDTag * 2);
                        index += (int)Transync.RFIDTag * 2;

                        model.StopByte = HexMessage.Substring(index, (int)Transync.StopByte * 2);
                        index += (int)Transync.StopByte * 2;

                        string statusreve = DBHelper.ReverseString(model.StatusByte);
                        statusModel = new TransyncStatusInfo();
                        statusModel = GetStatusInfo(statusreve); */
                     
                        

                    }
                }
            }
        }

        private static TcpDataPacket DecodeTcpPacket(byte[] request)
        {
            var reader = new ReverseBinaryReader(new MemoryStream(request));
            var decoder = new DataDecoder(reader);

            return decoder.DecodeTcpData();
        }
        //Getting Status Inforamtion
        public ProtocolStatusInfo GetStatusInfo(string _statusreve)
        {
            try
            {

                //===========================byte0=================================================
                //GPS Fix bit0
                protocolStatusInfo.GPS = _statusreve.Substring((int)Status_BYTE0.Bit0, 1).ToString() == "0" ? "V" : "A";
                //South&North
                protocolStatusInfo.SNLat = _statusreve.Substring((int)Status_BYTE0.Bit1, 1) == "0" ? "S" : "N";
                //West&East
                protocolStatusInfo.WELng = _statusreve.Substring((int)Status_BYTE0.Bit2, 1) == "0" ? "W" : "E";
                //Ign
                protocolStatusInfo.Ign = _statusreve.Substring((int)Status_BYTE0.Bit3, 1) == "0" ? "OF" : "ON";
                //Power
                protocolStatusInfo.Power = _statusreve.Substring((int)Status_BYTE0.Bit4, 1) == "0" ? "ON" : "OF";
                //AC
                protocolStatusInfo.AC = _statusreve.Substring((int)Status_BYTE0.Bit5, 1) == "0" ? "OF" : "ON";
                //out1
                protocolStatusInfo.Out1 = _statusreve.Substring((int)Status_BYTE0.Bit6, 1) == "0" ? "OF" : "ON";
                //out2
                protocolStatusInfo.Out2 = _statusreve.Substring((int)Status_BYTE0.Bit7, 1) == "0" ? "OF" : "ON";
                //=========================byte1========================================================
                //all bits are reserved
                //=========================byte2========================================================//Alerts
                //bit 0,1,2,3 are reserved
                protocolStatusInfo.Alerts.Immobilization = _statusreve.Substring((int)Status_BYTE2.Bit20, 1) == "0" ? false : true;
                protocolStatusInfo.Alerts.LowBattery = _statusreve.Substring((int)Status_BYTE2.Bit21, 1) == "0" ? false : true;
                protocolStatusInfo.Alerts.SOS = _statusreve.Substring((int)Status_BYTE2.Bit22, 1) == "0" ? false : true;
                protocolStatusInfo.Alerts.Overspeed = _statusreve.Substring((int)Status_BYTE2.Bit23, 1) == "0" ? false : true;
                //=========================byte3========================================================
                //bit4 is reserved
                protocolStatusInfo.DataType = _statusreve.Substring((int)Status_BYTE3.Bit31, 1) == "0" ? "N1" : "N2";
                protocolStatusInfo.DeviceType = DBHelper.GetDeviceType(_statusreve.Substring((int)Status_BYTE3.Bit24, 3));
            }
            catch (Exception)
            {
                // Logs.ExcetpionLog(ex.ToString());
            }
            return protocolStatusInfo;
        }
    }
}