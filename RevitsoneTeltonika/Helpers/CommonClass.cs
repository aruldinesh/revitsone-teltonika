﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace TCPListener.Helpers
{
    class CommonClass
    {
        public void SendEmail(string toAddress, string toCC, string mailSubject, string mailBody, bool isBodyHtml = false, Byte[] attachement = null, string attachmentName = null, bool isEnableSSL = false)
        {
            string reportSenderEmailId = ConfigurationManager.AppSettings["ReportSenderEmailId"].ToString();
            string reportSendingDisplayCompanyName = ConfigurationManager.AppSettings["ReportSendingDisplayCompanyName"].ToString();
            string reportSenderEmailPassword = ConfigurationManager.AppSettings["ReportSenderEmailPassword"].ToString();
            string emailHost = ConfigurationManager.AppSettings["EmailHost"].ToString();
            string emailPort = ConfigurationManager.AppSettings["EmailPort"].ToString();
            string overrideEmailId = ConfigurationManager.AppSettings["OverrideEmailId"].ToString();

            var fromAddress = new MailAddress(reportSenderEmailId, reportSendingDisplayCompanyName);
            var toMailAddress = new MailAddress(toAddress);
            MailAddress toCCAddress = null;
            if (!string.IsNullOrEmpty(overrideEmailId))
            {
                toMailAddress = new MailAddress(overrideEmailId);
            }
            else
            {
                if (!string.IsNullOrEmpty(toCC))
                {
                    toCCAddress = new MailAddress(toCC);
                }
            }

            string fromPassword = reportSenderEmailPassword;
            var smtp = new SmtpClient
            {
                Host = emailHost,
                Port = Convert.ToInt32(emailPort),
                EnableSsl = isEnableSSL,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            MailMessage message = new MailMessage(fromAddress, toMailAddress);
            if (toCCAddress != null)
            {
                message.CC.Add(toCCAddress);
            }
            message.Subject = mailSubject;
            if (attachement != null)
            {
                message.Attachments.Add(new Attachment(new MemoryStream(attachement), attachmentName));
            }

            message.Body = mailBody;
            message.IsBodyHtml = isBodyHtml;
            smtp.Send(message);
            message.Dispose();
        }
    }
}
