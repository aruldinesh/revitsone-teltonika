﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;

namespace CommonLibrary
{

    public class DBHelper : IDisposable
    {
        private static AppSettingsReader settingsReader = new AppSettingsReader();
        private static readonly Dictionary<char, string> hexCharacterToBinary = new Dictionary<char, string> {
    { '0', "0000" },
    { '1', "0001" },
    { '2', "0010" },
    { '3', "0011" },
    { '4', "0100" },
    { '5', "0101" },
    { '6', "0110" },
    { '7', "0111" },
    { '8', "1000" },
    { '9', "1001" },
    { 'a', "1010" },
    { 'b', "1011" },
    { 'c', "1100" },
    { 'd', "1101" },
    { 'e', "1110" },
    { 'f', "1111" }
};
        public static int executequery(string queryString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void executereader(string queryString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object executescalar(string queryString, bool is_sp = false, SqlParameter[] param = null)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    if (is_sp)
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(param);
                    }
                    command.Connection.Open();
                    return command.ExecuteScalar();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //[Obsolete("Use New Version with Textonly email version")]
        public static DataTable executeqry(string queryString, bool IS_SP = false)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {

                    SqlDataAdapter dataad = new SqlDataAdapter(queryString, connection);
                    if (IS_SP)
                    {
                        dataad.SelectCommand.CommandType = CommandType.StoredProcedure;
                    }
                    DataTable dt = new DataTable();
                    dataad.SelectCommand.Connection.Open();
                    dataad.Fill(dt);
                    return dt;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static DataTable executeqry(string queryString, SqlParameter[] param)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {

                    SqlDataAdapter dataad = new SqlDataAdapter(queryString, connection);
                    dataad.SelectCommand.CommandType = CommandType.StoredProcedure;
                    dataad.SelectCommand.Parameters.AddRange(param);
                    DataTable dt = new DataTable();
                    dataad.SelectCommand.Connection.Open();
                    dataad.Fill(dt);
                    return dt;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int executesp(string queryString, SqlParameter[] param)
        {

            using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
            {
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = queryString;
                    command.Parameters.AddRange(param);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 120;
                    //DB2Command command = new DB2Command(queryString, connection);
                    command.Connection.Open();
                    return command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    string errorMes = ex.Message;
                    return 0;
                }
            }
        }

        public static object executesp(string queryString, SqlParameter[] parameter, string returnparameter)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {

                    using (SqlCommand cmd = new SqlCommand(queryString))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameter);
                        connection.Open();
                        cmd.Connection = connection;
                        if (cmd.CommandText == "sp_FenceLog")
                        { cmd.CommandTimeout = 120; }
                        cmd.ExecuteNonQuery();
                        connection.Close();
                        return cmd.Parameters[returnparameter].Value;
                    }
                }
            }
            catch (Exception)
            { throw; }
        }

        public static object executesp(string queryString, SqlParameterCollection parameter, string returnparameter)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection((string)settingsReader.GetValue("GPSSERVER", typeof(String))))
                {

                    using (SqlCommand cmd = new SqlCommand(queryString))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(parameter);
                        connection.Open();
                        cmd.Connection = connection;
                        cmd.ExecuteNonQuery();
                        connection.Close();
                        return cmd.Parameters[returnparameter].Value;
                    }
                }
            }
            catch (Exception)
            { throw; }
        }
        public static string HexStringToString(string HexString)
        {
            string stringValue = "";
            for (int i = 0; i < HexString.Length / 2; i++)
            {
                string hexChar = HexString.Substring(i * 2, 2);
                int hexValue = Convert.ToInt32(hexChar, 16);
                stringValue += Char.ConvertFromUtf32(hexValue);
            }
            return stringValue;
        }
        public static string ParseHexDegrees(string hexNumber)
        {
            hexNumber = hexNumber.Replace("x", string.Empty);
            long result, degree = 0;
            float basevsl, minutes=0;
            //Converted to decimal
            long.TryParse(hexNumber, System.Globalization.NumberStyles.HexNumber, null, out result);
            //Extract Data
            if (result != 0)
            {
                basevsl = (float)result / 30000;
                degree = (long)basevsl / 60;
                minutes = (float)basevsl - (degree * 60);
               // minutes = (double)basevsl % 60;
             

            }
            return string.Format("{0},{1}", degree, minutes.ToString("#.##"));
        }
        public static double ConvertDegreeAngleToDouble(string point)
        {
            //Example: 17.21.18S

            var multiplier = (point.Contains("S") || point.Contains("W")) ? -1 : 1; //handle south and west

            point = Regex.Replace(point, "[^0-9.]", ","); //remove the characters

            var pointArray = point.Split(','); //split the string.

            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            var degrees =  pointArray[1]!=string.Empty?Double.Parse(pointArray[0]):0;
            var minutes = pointArray[1]!=string.Empty? Double.Parse(pointArray[1]) / 60:0;
           // var seconds = Double.Parse(pointArray[2]) / 3600;

            return Math.Round((degrees + minutes) * multiplier,6);
        }
        public static string HextoDatetime(string hexNumber)
        {
            string stringValue = "";
            for (int i = 0; i < hexNumber.Length / 2; i++)
            {
                string hexChar = hexNumber.Substring(i * 2, 2);
                int hexValue = Convert.ToInt32(hexChar, 16);
                switch (i)
                {
                    case 0:
                        stringValue += "20" + hexValue.ToString();
                        break;
                    case 1:
                        stringValue += "-" + hexValue.ToString("00");
                        break;
                    case 2:
                        stringValue += "-" + hexValue.ToString("00");
                        break;
                    case 3:
                        stringValue += " " + hexValue.ToString();
                        break;
                    case 4:
                        stringValue += ":" + hexValue.ToString();
                        break;
                    case 5:
                        stringValue += ":" + hexValue.ToString();
                        break;
                }


            }

            DateTime date = Convert.ToDateTime(stringValue);
            return date.ToString();
        }
        public static string HexStringToBinary(string hex)
        {
            StringBuilder result = new StringBuilder();
            foreach (char c in hex)
            {
                // This will crash for non-hex characters. You might want to handle that differently.
                result.Append(hexCharacterToBinary[char.ToLower(c)]);
            }
            return result.ToString();
        }
        public static float ParseHexSpeed(string hexNumber)
        {
            hexNumber = hexNumber.Replace("x", string.Empty);
            long result = 0;
            long.TryParse(hexNumber, System.Globalization.NumberStyles.HexNumber, null, out result);
           
            return result * (float)0.539957;//convert to Knot
        }
        public static List<string> GetChunkss(string value, int chunkSize)
        {
            List<string> triplets = new List<string>();
            for (int i = 0; i < value.Length; i += chunkSize)
                if (i + chunkSize > value.Length)
                    triplets.Add(value.Substring(i));
                else
                    triplets.Add(value.Substring(i, chunkSize));

            return triplets;
        }
        public static string GetDeviceType(string p)
        {

            string ReturnVal = string.Empty;
            switch (Convert.ToInt32(p, 2))
            {
                case 1:
                    ReturnVal = Constants.P2BasicTracker;
                    break;
                case 2:
                    ReturnVal = Constants.AssetTracker;
                    break;
                case 3:
                    ReturnVal = Constants.BikeTracker;
                    break;
                case 4:
                    ReturnVal = Constants.P2SerialDevice;
                    break;
                case 5:
                    ReturnVal = Constants.OBDDevice;
                    break;
                case 6:
                    ReturnVal = Constants.L1Multifunctional;
                    break;

            }
            return ReturnVal;
        }
        public static string ReverseString(string str)
        {

            char[] chars = str.ToCharArray();

            char[] result = new char[chars.Length];

            for (int i = 0, j = str.Length - 1; i < str.Length; i++, j--)
            {

                result[i] = chars[j];

            }

            return new string(result);

        }
        public static string ASCIIToHex(byte[] bytes)
        {
            var builder = new StringBuilder();

            var hexCharacters = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

            for (var i = 0; i < bytes.Length; i++)
            {
                int firstValue = (bytes[i] >> 4) & 0x0F;
                int secondValue = bytes[i] & 0x0F;

                char firstCharacter = hexCharacters[firstValue];
                char secondCharacter = hexCharacters[secondValue];

                //builder.Append("0x");
                builder.Append(firstCharacter);
                builder.Append(secondCharacter);
                // builder.Append(' ');
            }

            return builder.ToString().Trim(' ');
        }
        public static byte[] TrimEnd(byte[] array)
        {
            int lastIndex = Array.FindLastIndex(array, b => b != 0);

            Array.Resize(ref array, lastIndex + 1);

            return array;
        }
        #region IDisposable Members
        public void Dispose()
        {
        }
        #endregion
    }
}
