﻿using Genius.Google.Net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class DataAccess
    {
        //ProtocolModelView protocolModelView;
        //ProtocolStatusInfo protocolStatusInfo;
        //public DataAccess()
        //{
        //    protocolModelView = new ProtocolModelView();
        //    protocolStatusInfo = new ProtocolStatusInfo();
        //}
        //Split the Message
       /* public void MessageSplit(string HexMessage, string LogDirectory)
        {
            try
            {

                string[] splitdata = HexMessage.Split(new string[] { "2323" }, StringSplitOptions.None);
                for (int i = 0; i < splitdata.Length - 1; i++)
                {
                    string valuesdata = splitdata[i].TrimStart(new char[] { '0' }) + "2323";
                    Console.WriteLine(valuesdata);

                    int index = 0, total = HexMessage.Length;

                    model.StartByte = HexMessage.Substring(0, (int)Transync.StartByte * 2);
                    index += (int)Transync.StartByte * 2;

                    model.PacketLegnth = HexMessage.Substring(index, (int)Transync.PacketLegnth * 2);
                    index += (int)Transync.PacketLegnth * 2;

                    model.LAC = HexMessage.Substring(index, (int)Transync.LAC * 2);
                    index += (int)Transync.LAC * 2;

                    model.DeviceID = (HexMessage.Substring(index, (int)Transync.DeviceID * 2)).TrimStart(new char[] { '0' });
                    index += (int)Transync.DeviceID * 2;

                    model.InfoSerialNo = HexMessage.Substring(index, (int)Transync.InfoSerialNo * 2);
                    index += (int)Transync.InfoSerialNo * 2;

                    model.ProtocolNumber = HexMessage.Substring(index, (int)Transync.ProtocolNumber * 2);
                    index += (int)Transync.ProtocolNumber * 2;

                    model.DateTime = DBHelper.HextoDatetime(HexMessage.Substring(index, (int)Transync.DateTime * 2));
                    index += (int)Transync.DateTime * 2;

                    model.Latitude = Convert.ToString(DBHelper.ParseHexDegrees(HexMessage.Substring(index, (int)Transync.Latitude * 2)));
                    index += (int)Transync.Latitude * 2;

                    model.Longitude = Convert.ToString(DBHelper.ParseHexDegrees(HexMessage.Substring(index, (int)Transync.Longitude * 2)));
                    index += (int)Transync.Longitude * 2;

                    model.Speed = Convert.ToString(DBHelper.ParseHexSpeed(HexMessage.Substring(index, (int)Transync.Speed * 2)));
                    index += (int)Transync.Speed * 2;

                    model.Course = HexMessage.Substring(index, (int)Transync.Course * 2);
                    index += (int)Transync.Course * 2;

                    model.MNC = HexMessage.Substring(index, (int)Transync.MNC * 2);
                    index += (int)Transync.MNC * 2;

                    model.CellID = HexMessage.Substring(index, (int)Transync.CellID * 2);
                    index += (int)Transync.CellID * 2;

                    model.StatusByte = DBHelper.HexStringToBinary(HexMessage.Substring(index, (int)Transync.StatusByte * 2));
                    index += (int)Transync.StatusByte * 2;

                    model.GSMSignalStregnth = HexMessage.Substring(index, (int)Transync.GSMSignalStregnth * 2);
                    index += (int)Transync.GSMSignalStregnth * 2;

                    model.VoltageLevel = HexMessage.Substring(index, (int)Transync.VoltageLevel * 2);
                    index += (int)Transync.VoltageLevel * 2;

                    model.HDOP = HexMessage.Substring(index, (int)Transync.HDOP * 2);
                    index += (int)Transync.HDOP * 2;

                    model.ADCValue = HexMessage.Substring(index, (int)Transync.ADCValue * 2);
                    index += (int)Transync.ADCValue * 2;

                    model.OdometerIndex = HexMessage.Substring(index, (int)Transync.OdometerIndex * 2);
                    index += (int)Transync.OdometerIndex * 2;

                    model.OdometerLegnth = HexMessage.Substring(index, (int)Transync.OdometerLegnth * 2);
                    index += (int)Transync.OdometerLegnth * 2;

                    model.OdometerReading = HexMessage.Substring(index, (int)Transync.OdometerReading * 2);
                    index += (int)Transync.OdometerReading * 2;

                    model.RFIDIndex = HexMessage.Substring(index, (int)Transync.RFIDIndex * 2);
                    index += (int)Transync.RFIDIndex * 2;

                    model.RFIDTag = HexMessage.Substring(index, (int)Transync.RFIDTag * 2);
                    index += (int)Transync.RFIDTag * 2;

                    model.StopByte = HexMessage.Substring(index, (int)Transync.StopByte * 2);
                    index += (int)Transync.StopByte * 2;

                    string statusreve = DBHelper.ReverseString(model.StatusByte);
                    statusModel = new TransyncStatusInfo();
                    statusModel = GetStatusInfo(statusreve);
                    if (model != null && statusModel != null)
                       FunInsertDb(model, statusModel);

                }

            }
            catch (Exception ex)
            {
                //Logs.ExcetpionLog(ex.ToString(),ExceptionDirectory);
            }

        } */

        //Insert to Database
        public void InsertTCPMessage(ProtocolModelView protocolModelView, ProtocolStatusInfo protocolStatusInfo)
        {
            try
            {
                DateTime gmtdate = DateTime.UtcNow;
                string IOpins = "0" + (protocolStatusInfo.Ign == "ON" ? "1" : "0") + "00" + (protocolStatusInfo.Alerts.SOS == true ? "1" : "0") + (protocolStatusInfo.Power == "ON" ? "0" : "1") + "00";
                //double lat = protocolModelView.Latitude;// DBHelper.ConvertDegreeAngleToDouble(string.Format("{0}{1}", protocolModelView.Latitude, protocolStatusInfo.SNLat));
                //double lng=DBHelper.ConvertDegreeAngleToDouble(string.Format("{0}{1}",protocolModelView.Longitude,protocolStatusInfo.WELng));
                if (protocolStatusInfo.DataType == "N1")
                {
                    int loc = location(Convert.ToDouble(protocolModelView.Longitude), Convert.ToDouble(protocolModelView.Latitude), protocolModelView.DeviceID, gmtdate, protocolStatusInfo.GPS);

                    SqlParameter[] live_parm = 
                                        {
                                        new SqlParameter{ ParameterName="@tmnlid",Value = protocolModelView.DeviceID },
                                        new SqlParameter{ ParameterName="@msgtype",Value=protocolStatusInfo.Alerts.Overspeed?"O":protocolStatusInfo.DataType },
                                        new SqlParameter{ ParameterName="@indate",Value  = protocolModelView.DateTime },
                                        new SqlParameter{ ParameterName="@va",Value=protocolStatusInfo.GPS},
                                        new SqlParameter{ ParameterName="@lattitude",Value = protocolModelView.Longitude },
                                        new SqlParameter{ ParameterName="@longitude",Value = protocolModelView.Latitude},
                                        new SqlParameter{ ParameterName="@lcid",Value =loc},
                                        new SqlParameter{ ParameterName="@speed", Value =  protocolModelView.Speed},
                                        new SqlParameter{ ParameterName="@gmtdate",Value = gmtdate},
                                        new SqlParameter{ ParameterName="@gmttime",Value = gmtdate.ToString("HH:mm:ss") },
                                        new SqlParameter{ ParameterName="@direction",Value = protocolModelView.Course},
                                        new SqlParameter{ ParameterName="@iopins",Value = IOpins },
                                        new SqlParameter{ ParameterName="@distance",Value = "0"},
                                        new SqlParameter{ ParameterName="@msgval",Value = "0"},
                                        new SqlParameter{ ParameterName="@tstatus",Value = protocolStatusInfo.Ign == "ON" ? 0 : 1},
                                        new SqlParameter{ ParameterName="@errmsg",Value = "0"},
                                        new SqlParameter{ ParameterName="@fuel",Value ="0" },
                                        new SqlParameter{ ParameterName="@temp",Value ="0" }
                                        };
                    try
                    {
                        int returnValue = DBHelper.executesp("SP_PROCESS_TCP_MESSAGE", live_parm);
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                    
                }
                else
                {
                    int loc = location_mem(Convert.ToDouble(protocolModelView.Longitude), Convert.ToDouble(protocolModelView.Latitude), protocolModelView.DeviceID, gmtdate, protocolStatusInfo.GPS);

                    SqlParameter[] mem_param = 
                                        {
                                        new SqlParameter{ ParameterName="@tmnlid",Value =  protocolModelView.DeviceID },
                                        new SqlParameter{ ParameterName="@msgtype",Value = protocolStatusInfo.Alerts.Overspeed?"O":protocolStatusInfo.DataType },
                                        new SqlParameter{ ParameterName="@indate",Value  = protocolModelView.DateTime},
                                        new SqlParameter{ ParameterName="@va",Value = protocolStatusInfo.GPS},
                                        new SqlParameter{ ParameterName="@lattitude",Value = protocolModelView.Longitude  },
                                        new SqlParameter{ ParameterName="@longitude",Value = protocolModelView.Latitude},
                                        new SqlParameter{ ParameterName="@lcid",Value =loc },
                                        new SqlParameter{ ParameterName="@speed", Value =  protocolModelView.Speed },
                                        new SqlParameter{ ParameterName="@gmtdate",Value = gmtdate},
                                        new SqlParameter{ ParameterName="@gmttime",Value =gmtdate.ToString("HH:mm:ss")},
                                        new SqlParameter{ ParameterName="@direction",Value = 0},
                                        //new SqlParameter{ ParameterName="@iopins",Value = ToBinary(val[8]) },
                                        new SqlParameter{ ParameterName="@iopins",Value = IOpins },
                                        new SqlParameter{ ParameterName="@distance",Value = 0},
                                        new SqlParameter{ ParameterName="@msgval",Value = '0'},
                                        new SqlParameter{ ParameterName="@tstatus",Value = protocolStatusInfo.Ign == "ON" ? 0 : 1},
                                        new SqlParameter{ ParameterName="@errmsg",Value = "0"},
                                        new SqlParameter{ ParameterName="@fuel",Value ="0" },
                                          new SqlParameter{ ParameterName="@temp",Value ="0" }
                                        };

                    DBHelper.executesp("sp_messageprocessing_memory", mem_param);
                }
            }
            catch (Exception)
            {
                //Logs.ExcetpionLog(ex.ToString());
            }
        }
        //Getting Status Inforamtion
       /* public TransyncStatusInfo GetStatusInfo(string _statusreve)
        {
            try
            {

                //===========================byte0=================================================
                //GPS Fix bit0
                protocolStatusInfo.GPS = _statusreve.Substring((int)Status_BYTE0.Bit0, 1).ToString() == "0" ? "V" : "A";
                //South&North
                protocolStatusInfo.SNLat = _statusreve.Substring((int)Status_BYTE0.Bit1, 1) == "0" ? "S" : "N";
                //West&East
                protocolStatusInfo.WELng = _statusreve.Substring((int)Status_BYTE0.Bit2, 1) == "0" ? "W" : "E";
                //Ign
                protocolStatusInfo.Ign = _statusreve.Substring((int)Status_BYTE0.Bit3, 1) == "0" ? "OF" : "ON";
                //Power
                protocolStatusInfo.Power = _statusreve.Substring((int)Status_BYTE0.Bit4, 1) == "0" ? "ON" : "OF";
                //AC
                protocolStatusInfo.AC = _statusreve.Substring((int)Status_BYTE0.Bit5, 1) == "0" ? "OF" : "ON";
                //out1
                protocolStatusInfo.Out1 = _statusreve.Substring((int)Status_BYTE0.Bit6, 1) == "0" ? "OF" : "ON";
                //out2
                protocolStatusInfo.Out2 = _statusreve.Substring((int)Status_BYTE0.Bit7, 1) == "0" ? "OF" : "ON";
                //=========================byte1========================================================
                //all bits are reserved
                //=========================byte2========================================================//Alerts
                //bit 0,1,2,3 are reserved
                protocolStatusInfo.Alerts.Immobilization = _statusreve.Substring((int)Status_BYTE2.Bit20, 1) == "0" ? false : true;
                protocolStatusInfo.Alerts.LowBattery = _statusreve.Substring((int)Status_BYTE2.Bit21, 1) == "0" ? false : true;
                protocolStatusInfo.Alerts.SOS = _statusreve.Substring((int)Status_BYTE2.Bit22, 1) == "0" ? false : true;
                protocolStatusInfo.Alerts.Overspeed = _statusreve.Substring((int)Status_BYTE2.Bit23, 1) == "0" ? false : true;
                //=========================byte3========================================================
                //bit4 is reserved
                protocolStatusInfo.DataType = _statusreve.Substring((int)Status_BYTE3.Bit31, 1) == "0" ? "N1" : "N2";
                protocolStatusInfo.DeviceType = DBHelper.GetDeviceType(_statusreve.Substring((int)Status_BYTE3.Bit24, 3));
            }
            catch (Exception)
            {
                // Logs.ExcetpionLog(ex.ToString());
            }
            return protocolStatusInfo;
        } */
        private int location(double lat, double lng, string tmnlid, DateTime indate, string valid)
        {
            //if (valid == "V") { return 0; }
            //if (lat == 0 && lng == 0) { return 0; }
            int lcid = 0;
            string res;
            // BingRequest req = new BingRequest();
            // string bingloc = req.CreateRequest(lat, lng);
            //    string googleloc = getloc(lat, lng);
            try
            {
                object marker = DBHelper.executescalar("select lcid from markermst where (( 6371 * acos( cos( radians( " + lat + ") ) * cos( radians(LAT) ) * cos( radians( " + lng + " ) - radians( LNG ) ) + sin( radians(" + lat + ") ) * sin( radians(LAT) ) ) )*1000) <= 30  and MRK_TYPE = 'M' and E_stat=1");

                if (marker == null)
                {

                    // object result = Connections.executescalar("select ISNULL( RECNO , 0) from GPS_LOC where LAT =  round( " + lat + ", 3, 1)  and LNG = round( " + lng + ", 3, 1)");

                    object result = DBHelper.executescalar("select ISNULL( RECNO , 0) from GPS_LOC where LAT =  convert( numeric(20,3)," + lat + ") and Lng=convert( numeric(20,3)," + lng + ")  and location!='unknown' order by recno desc");
                    //object result = Connections.executescalar("select ISNULL( RECNO , 0) from GPS_LOC where LAT = " + lat.ToString().Substring(0, lat.ToString().IndexOf('.') + (((lat.ToString().Length - lat.ToString().IndexOf('.')) > 4) ? 4 : (lat.ToString().Length - lat.ToString().IndexOf('.')))) + " and LNG = " + lng.ToString().Substring(0, lng.ToString().IndexOf('.') + (((lng.ToString().Length - lng.ToString().IndexOf('.')) > 4) ? 4 : (lng.ToString().Length - lng.ToString().IndexOf('.')))));
                    //res = Connections.executescalar("select location from gps_loc where recno=" + Convert.ToInt32(result)).ToString();
                    if (result != null)
                    {
                        res = DBHelper.executescalar("select location from gps_loc where recno=" + Convert.ToInt32(result)).ToString();
                        if (res != "Unknown")
                        {
                            lcid = Convert.ToInt32(result);

                        }

                    }
                    if (lcid <= 0)
                    {
                        // string loc = getloc(lat, lng);
                        //string loc = GetAddress(lat, lng);
                        var sqlParams = new List<SqlParameter>();
                        sqlParams.Add(new SqlParameter { ParameterName = "@LAT", Value = lat });
                        sqlParams.Add(new SqlParameter { ParameterName = "@LNG", Value = lng });
                        //sqlParams.Add(new SqlParameter { ParameterName = "@LOCATION", Value = loc });


                        //sqlParams.Add(new SqlParameter { ParameterName = "@LOCATION", Value = bingloc(lat, lng) });
                        sqlParams.Add(new SqlParameter { ParameterName = "@LOCATION", Value = getloc(lat, lng) });

                        sqlParams.Add(new SqlParameter { ParameterName = "@RECNO", Value = 0, Direction = ParameterDirection.ReturnValue });
                        SqlParameter[] para = sqlParams.ToArray();
                        lcid = (int)DBHelper.executesp("sp_insert_gpsloc", para, "@RECNO");
                    }
                }
                else
                {
                    lcid = Convert.ToInt32(marker);
                }
                //res = Connections.executescalar("select location from gps_loc where recno=" + lcid).ToString();

                // if (res != "Unknown" && lcid <= 0)


                markerlog(lat, lng, tmnlid, indate);


            }
            catch (Exception)
            {
                throw;
            }
            return lcid;
        }

        private int location_mem(double lat, double lng, string tmnlid, DateTime indate, string valid)
        {
            //if (valid == "V") { return 0; }
            //if (lat == 0 && lng == 0) { return 0; }
            int lcid = 0;
            string res;
            // BingRequest req = new BingRequest();
            // string bingloc = req.CreateRequest(lat, lng);
            //    string googleloc = getloc(lat, lng);
            try
            {
                object marker = DBHelper.executescalar("select lcid from markermst where (( 6371 * acos( cos( radians( " + lat + ") ) * cos( radians(LAT) ) * cos( radians( " + lng + " ) - radians( LNG ) ) + sin( radians(" + lat + ") ) * sin( radians(LAT) ) ) )*1000) <= 30  and MRK_TYPE = 'M' and E_stat=1");

                if (marker == null)
                {

                    // object result = Connections.executescalar("select ISNULL( RECNO , 0) from GPS_LOC where LAT =  round( " + lat + ", 3, 1)  and LNG = round( " + lng + ", 3, 1)");

                    object result = DBHelper.executescalar("select ISNULL( RECNO , 0) from GPS_LOC where LAT =  convert( numeric(20,3)," + lat + ") and Lng=convert( numeric(20,3)," + lng + ")  and location!='unknown' order by recno desc");
                    //object result = Connections.executescalar("select ISNULL( RECNO , 0) from GPS_LOC where LAT = " + lat.ToString().Substring(0, lat.ToString().IndexOf('.') + (((lat.ToString().Length - lat.ToString().IndexOf('.')) > 4) ? 4 : (lat.ToString().Length - lat.ToString().IndexOf('.')))) + " and LNG = " + lng.ToString().Substring(0, lng.ToString().IndexOf('.') + (((lng.ToString().Length - lng.ToString().IndexOf('.')) > 4) ? 4 : (lng.ToString().Length - lng.ToString().IndexOf('.')))));
                    //res = Connections.executescalar("select location from gps_loc where recno=" + Convert.ToInt32(result)).ToString();
                    if (result != null)
                    {
                        res = DBHelper.executescalar("select location from gps_loc where recno=" + Convert.ToInt32(result)).ToString();
                        if (res != "Unknown")
                        {
                            lcid = Convert.ToInt32(result);

                        }

                    }
                    if (lcid <= 0)
                    {
                        // string loc = getloc(lat, lng);
                        //string loc = GetAddress(lat, lng);
                        var sqlParams = new List<SqlParameter>();
                        sqlParams.Add(new SqlParameter { ParameterName = "@LAT", Value = lat });
                        sqlParams.Add(new SqlParameter { ParameterName = "@LNG", Value = lng });
                        //sqlParams.Add(new SqlParameter { ParameterName = "@LOCATION", Value = loc });


                        //sqlParams.Add(new SqlParameter { ParameterName = "@LOCATION", Value = bingloc(lat, lng) });
                        sqlParams.Add(new SqlParameter { ParameterName = "@LOCATION", Value = getloc(lat, lng) });

                        sqlParams.Add(new SqlParameter { ParameterName = "@RECNO", Value = 0, Direction = ParameterDirection.ReturnValue });
                        SqlParameter[] para = sqlParams.ToArray();
                        lcid = (int)DBHelper.executesp("sp_insert_gpsloc", para, "@RECNO");
                    }
                }
                else
                {
                    lcid = Convert.ToInt32(marker);
                }



            }
            catch (Exception)
            {
                throw;
            }
            return lcid;
        }

        private void markerlog(double lat, double lng, string tmnlid, DateTime indate)
        {
            try
            {
                SqlParameter[] param = 
                    {
                    new SqlParameter{ ParameterName="@lat",Value = lat},
                    new SqlParameter{ ParameterName="@lng",Value = lng},
                    new SqlParameter{ ParameterName="@terminal_id",Value = tmnlid},
                    new SqlParameter{ ParameterName="@msgdate",Value = indate.ToString("yyyy-MMM-dd HH:mm:ss")}
                    };
                DBHelper.executesp("sp_markerlog", param);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private string getloc(double plat, double plng)
        {
            try
            {

                GeoRequest objreq = new GeoRequest(plat, plng);
                GeoResponse objres = objreq.GetResponse();
                if (objres.Results.Count > 0)
                { return objres.Results[0].FormattedAddress; }
                else
                { return "Unknown"; }
            }
            catch (FormatException)
            {
                // Logs.ExcetpionLog(fe.ToString());
                return "Unknown";
            }
            catch (Exception)
            {
                //Logs.ExcetpionLog(ex.ToString());
                return "Unknown";
            }
        }

    }
    public class TransyncModelView
    {
        //Allocated on bytes
        public string StartByte { get; set; }
        public string PacketLegnth { get; set; }
        public string LAC { get; set; }
        public string DeviceID { get; set; }
        public string InfoSerialNo { get; set; }
        public string ProtocolNumber { get; set; }
        public string DateTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Speed { get; set; }
        public string Course { get; set; }
        public string MNC { get; set; }
        public string CellID { get; set; }
        public string StatusByte { get; set; }
        public string GSMSignalStregnth { get; set; }
        public string VoltageLevel { get; set; }
        public string NoOfSatellite { get; set; }
        public string HDOP { get; set; }
        public string ADCValue { get; set; }
        public string OdometerIndex { get; set; }
        public string OdometerLegnth { get; set; }
        public string OdometerReading { get; set; }
        public string RFIDIndex { get; set; }
        public string RFIDLegnth { get; set; }
        public string RFIDTag { get; set; }
        public string StopByte { get; set; }

    }
    public class TransyncStatusInfo
    {
        public TransyncStatusInfo()
        {
            Alerts = new Alerts();
        }
        //byte0
        public string GPS { get; set; }
        public string SNLat { get; set; }
        public string WELng { get; set; }
        public string Ign { get; set; }
        public string Power { get; set; }
        public string AC { get; set; }
        public string Out1 { get; set; }
        public string Out2 { get; set; }
        //byte1
        //All Reserved
        //byte2
        public Alerts Alerts { get; set; }
        //byte3
        public string DeviceType { get; set; }
        public string DataType { get; set; }

    }

    public class ProtocolStatusInfo
    {
        public ProtocolStatusInfo()
        {
            Alerts = new Alerts();
        }
        //byte0
        public string GPS { get; set; }
        public string SNLat { get; set; }
        public string WELng { get; set; }
        public string Ign { get; set; }
        public string Power { get; set; }
        public string AC { get; set; }
        public string Out1 { get; set; }
        public string Out2 { get; set; }
        //byte1
        //All Reserved
        //byte2
        public Alerts Alerts { get; set; }
        //byte3
        public string DeviceType { get; set; }
        public string DataType { get; set; }

    }

    public class ProtocolModelView
    {
        //Allocated on bytes
        public string StartByte { get; set; }
        public string PacketLegnth { get; set; }
        public string LAC { get; set; }
        public string DeviceID { get; set; }
        public string InfoSerialNo { get; set; }
        public string ProtocolNumber { get; set; }
        public string DateTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Speed { get; set; }
        public string Course { get; set; }
        public string MNC { get; set; }
        public string CellID { get; set; }
        public string StatusByte { get; set; }
        public string GSMSignalStregnth { get; set; }
        public string VoltageLevel { get; set; }
        public string NoOfSatellite { get; set; }
        public string HDOP { get; set; }
        public string ADCValue { get; set; }
        public string OdometerIndex { get; set; }
        public string OdometerLegnth { get; set; }
        public string OdometerReading { get; set; }
        public string RFIDIndex { get; set; }
        public string RFIDLegnth { get; set; }
        public string RFIDTag { get; set; }
        public string StopByte { get; set; }

    }
}
