﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLibrary
{
    public class Alerts
    {
        //byte2
        public bool Immobilization { get; set; }
        public bool LowBattery { get; set; }
        public bool SOS { get; set; }
        public bool Overspeed { get; set; }

        //byte3
        public bool GPSNotRespond { get; set; }

    }
}
